package ictgradschool.industry.lab09;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Lab09TestCodes {

    public void start() {

        ArrayList list = new ArrayList();
        Character letter = new Character('a');
        list.add(letter);

        if (list.get(0).equals("a")) {
            System.out.println("funny");
        } else {
            System.out.println("Not funny"); // output: Not funny - "a" is a String while list.get(0) is a Character 'a'
        }

    }

    public void start2() {
        ArrayList<Point> list = new ArrayList<Point>();
        Point pt1 = new Point(3,4);
        list.add(pt1);
        Point pt2 = list.get(0);
        pt2.x = 23;
        if (pt2 == pt1) {
            System.out.println("Same object"); // they are the same object
        } else {
            System.out.println("Different object");
        }
    }

    public void start3() {
        ArrayList list = new ArrayList();
        list.add('a');
        list.add(0, 34);
//        String c1 = (String) list.get(1); // will result to ClassCastException error, character cannot be cast to String
        String c1 = list.get(1) + ""; // use string concatenation instead
        System.out.println(c1);
    }

    public void ex02a() {
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};

        List<String> myList = Arrays.asList(array);

        for (int i = 0; i < myList.size(); i++) {
            String s = myList.get(i).toLowerCase();
            myList.set(i, s);
        }

        System.out.println(myList);

    }

    public void ex02b() {
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};

        List<String> myList = Arrays.asList(array);

        int i = 0;
        for (String temp: myList) {
            String s = temp.toLowerCase();
            myList.set(i, s);
            i++;
        }

        System.out.println(myList);

    }

    public void ex02c() {
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};

        List<String> myList = Arrays.asList(array);

        ListIterator<String> myIterator = myList.listIterator();

        while (myIterator.hasNext()) {
            String element = myIterator.next().toLowerCase();
            myIterator.set(element);
//            int i = myList.indexOf(element);
//            myList.set(i, element.toLowerCase());
        }

        System.out.println(myList);

    }

    public void classExercise() {

        String[] letters = {"B", "C", "E", "A", "B", "C", "D"};

        List<String> lettersList1 = Arrays.asList(letters);

        //adding each element through a for loop
        List<String> lettersList2 = new ArrayList<>();
        for (int i = 0; i < letters.length; i++) {
            lettersList2.add(letters[i]);
        }

        //adding each element through a for loop
        Set<String> lettersSet =  new HashSet<>();
        for (int i = 0; i < letters.length; i++) {
            lettersSet.add(letters[i]);
        }

        for (String letter : lettersSet) {
            System.out.println(letter);
        }

    }

    public void printBackwards1(String s) {
        int size = s.length();
        if (size < 1) {
            return;
        }
        printBackwards1(s.substring(1));
        System.out.print(s.charAt(0));
    }

    public void testDeque() {

        Deque<String> myDeque = new ArrayDeque<>();
        myDeque.addLast("first");
        myDeque.addLast("second");
        myDeque.addLast("third");

        System.out.println(myDeque.pollFirst());
        System.out.println(myDeque.pollFirst());
        System.out.println(myDeque.pollFirst());
        System.out.println(myDeque.pollFirst());
//        System.out.println(myDeque.remove());
//        System.out.println(myDeque.remove());
//        System.out.println(myDeque.remove());
//        System.out.println(myDeque.remove()); // will cause NoSuchElementException
    }

    public void testSet() {

        int[] a = {1, 2, 3, 1};

        Set<Integer> intSet = new HashSet<>();

        int i = 0;
        while (intSet.add(a[i])) {
            i++;
        }

        System.out.println(intSet);
    }

    public static void main(String[] args) {
        Lab09TestCodes lab09 = new Lab09TestCodes();
//        lab09.start();
//        lab09.start2();
//        lab09.start3();
//        lab09.ex02a();
//        lab09.ex02b();
//        lab09.ex02c();
//        lab09.classExercise();
//        lab09.printBackwards1("cat");
        lab09.testDeque();
        lab09.testSet();
        System.out.println();

        List<Tutor> tutors = new ArrayList<>();
        tutors.add(new Tutor("male", "Jack", 18));
        tutors.add(new Tutor("female", "Nicole", 20));
        tutors.add(new Tutor("male", "Jarvis", 19));

        for (Tutor tutor : tutors) {
            System.out.println(tutor);
        }

        System.out.println("==========================");

//        Comparator<Tutor> comparator = new Comparator<Tutor>() {
//            @Override
//            public int compare(Tutor o1, Tutor o2) {
//                if (o1.gender.equals(o2.gender)){ // gender variable had to be made public in the Tutor class for this to work in the absence of a getter method
//                    return o1.age - o2.age;
//                }
//
//                return o1.gender.compareTo(o2.gender);
//            }
//        }

        Collections.sort(tutors);

        for (Tutor tutor : tutors) {
            System.out.println(tutor);
        }

        System.out.println("==========================");

        List<Tutor> myTutorList = new ArrayList<>();
        myTutorList.add(new Tutor("male", "Jack", 18));
        myTutorList.add(new Tutor("female", "Nicole", 20));
        myTutorList.add(new Tutor("male", "Jarvis", 19));

        Tutor tutorTest = myTutorList.get(0);
        tutorTest.setName("anotherTutor");
        System.out.println(myTutorList.get(0));

        for (Tutor t: myTutorList) {
            t = new Tutor("unknown", "Mickey", 5); // will not change List items
            t.setName("Mickey");
            System.out.println(t);
        }
        System.out.println(myTutorList);


    }
}
