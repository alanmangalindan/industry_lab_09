package ictgradschool.industry.lab09;

public class TestCodes2 {

    public void start() {

        Card c = new Card(Suit.Spades, Rank.Two);

        System.out.println(c.getSuit());
        System.out.println(c.getRank());

    }

    public static void main(String[] args) {
        new TestCodes2().start();
    }
}
