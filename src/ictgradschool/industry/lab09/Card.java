package ictgradschool.industry.lab09;

public class Card {
    private Suit suit;
    private Rank rank;

    public Card (Suit s, Rank r) {
        this.suit = s;
        this.rank = r;
    }

    public Suit getSuit() {
        return this.suit;
    }

    public Rank getRank() {
        return this.rank;
    }
}
