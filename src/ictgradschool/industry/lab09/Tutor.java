package ictgradschool.industry.lab09;

public class Tutor implements Comparable<Tutor> {

    private String gender;
    private String name;
    private int age;

    public Tutor(String gender, String name, int age) {
        this.gender = gender;
        this.name = name;
        this.age = age;
    }

    public void setName (String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name " + this.name + " age: " + this.age + " gender: " + this.gender;
    }


    @Override
    public int compareTo(Tutor o) {
        if (this.gender.equals(o.gender)){
            return this.age - o.age;
        }

        return this.gender.compareTo(o.gender); // changing this and o will reverse the sort order

    }
}
