package ictgradschool.industry.lab09;

public enum Suit {
    Spades, Clubs, Diamonds, Hearts;
}
